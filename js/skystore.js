(function() {
    'use strict';

    angular
        .module('skyStore', [
            'skyStore.routes',
            'skyStore.products',
            'ngAnimate',
            'ui.bootstrap',
            'rzModule',
            'ui.filters',
            'ngCart'
        ]);

    angular
        .module('skyStore.routes', ['ui.router']);

    angular
        .module('ui.filters', []);

    angular
        .module('skyStore')
        .run(function($animate) {
            $animate.enabled(true);
        });
})();