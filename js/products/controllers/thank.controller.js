(function(){
    'use strict';

    angular
        .module('products.controllers')
        .controller('ThanksController', ThanksController);

    ThanksController.$inject = ['$state', 'ngCart', '$location'];

    function ThanksController($state, ngCart, $location) {

        ngCart.empty();
        window.location.href = window.location.protocol + '//'  +window.location.host + '/'+ window.location.hash;
    }
})();