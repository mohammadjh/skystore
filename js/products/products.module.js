(function() {
    'use strict';

    angular
        .module('skyStore.products', [
        'products.controllers',
        'products.services',
        'products.filters'
        ]);

    angular
        .module('products.controllers', []);

    angular
        .module('products.services', []);

    angular
        .module('products.filters', []);

})();